Feature: CycleScript Standards Exercise

Background: Minimize Cycle
Given I minimize Cycle
      
After Scenario: Maximize Cycle
Given I maximize Cycle

@wip
Scenario: CycleScript Standards
# Convert CycleScript written in the past week to standard
# Otherwise, write some CycleScript written to standard
# Share for feedback
# 10:25AM

Scenario: Multiple Google Name Searches

Given I "Prompt User for Name and Times to Search"
	When I prompt "Enter Your Name!" and assign user response to variable "usr_name"
	And I prompt "Enter the number of times to search!" for integer and assign user response to variable "times_to_search"

And I open Chrome web browser

When I "Search for User's Name in Web Browser"
	While I verify number $times_to_search is greater than 0
    	Given I "navigate to Google"
			Given I navigate to "http://www.google.com" in web browser
			Once I see image "Image:images\google002.png" in web browser within 15 seconds
        
        When I "perform the Search"
			When I enter $usr_name
			And I press keys ENTER in web browser 1 times with 10 seconds delay
        
        Then I "have searched once and note it"
			Then I decrease variable "times_to_search"
			If I verify number $times_to_search is greater than 0
				Then I open new tab in web browser
			EndIf
	EndWhile

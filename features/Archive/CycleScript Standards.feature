Feature: CycleScript Standards

@wip
Scenario: What are CycleScript Standards?
Given I "know they are standards for developing consistent, readable, and maintainable CycleScript"

@wip
Scenario: What are the philosophical tenets?
#
# Ensure readability for technical and non-technical users.
#

Given I assign 0 to variable "x"
While I verify number $x is less than 5
Then I execute scenario "Subscenario"
And I increase variable "x" by 1
Endwhile 

# What is the below step? Activity Step.
Given I "unload the truck five times"
# Indentation is important.
	# Variable names are important.
	Given I assign 1 to variable "times_unloaded"
    While I verify number $times_unloaded is not equal to 5
    	# Scenario names are important.
    	Then I execute scenario "Unload the Truck"
        And I increase variable "times_unloaded" by 1
    Endwhile

#
# Validate state before action.
#
# Terminal Example
Once I verify screen is done loading in terminal within <NUMBER> seconds
Once I see "<TEXT>" in terminal
Once I see cursor at line <LINE_NUMBER> column <COLUMN_NUMBER> in terminal

#
# Use modularity to prevent code repetition.
#
# Utility Scenarios - reusable, modular, accept parameters if necessary


#
# Convention over configuration.
#

#
# Keep utilities consistent
#

@wip
Scenario: Keyword Usage
# Given
# When
# Then
# And 
# But

# Behavior-Driven Development

# User Story: A story of what a user does

Given I "verify the state of the application"
And I "transition the application to the right state"
And I "collect background data"
And I "import utilities"

When I "perform an action"
And I "perform a follow-up action"

Then I "validates the action"
And I "validate the action in another way like a database query"

But I "validate something that didn't happen"
And I "make sure something else didn't happen"

@wip
Scenario: Keyword Usage in a Control Structure like If-Else and While
While I "remove all my Facebook friends because I am quitting social media"
# Conjoint AND
And I "ensure that I am logged into Facebook"
	Given I "delete a Facebook Friend"
Endwhile

If I "see my archnemesis, John Villain"
And I "am in a bad mood"
	Then I "delete his profile"
Elsif I "see my best friend, John Not-Villain"
And I "am in a good mood"
	Then I "upgrade his profile"
Endif

Scenario: Business/Activity/Comment Steps
Given I "<ACTIVITY>"

##
Given I "open Herobook"
	When I open "Chrome" web browser
    And I navigate to "herobook.com" in web browser
    Then I see "Herobook" in web browser

And I "login to Herobook"
    When I type "Cycle Woman" in element "xPath://input[@id='loginID']" in web browser
    And I type "Iamawesome!!" in element "xPath://input[@id='password']" in web browser
    And I press keys "ENTER" in web browser
    Then I see "Hello Cycle Woman!" in web browser

And I "navigate to the Heroes page"
	When I navigate to "herobook.com/heroes" in web browser
    Then I see "List of Heroes" in web browser

And I "see Cycle Man as a Hero"

When I "select Cycle Man"

And I "click Add Herobuddy"

Then I "verify that he is my Herobuddy"

Scenario: User Story Substory
When I type "Cycle Woman" in element "xPath://input[@id='loginID']" in web browser

And I type "Iamawesome!!" in element "xPath://input[@id='password']" in web browser

And I press keys "ENTER" in web browser

Then I see "Hello Cycle Woman!" in web browser

Scenario: CycleScript Formatting Rules
# Use Forward Slashes
# Use Business/Activity/Comments Steps
# Use indentation with Control Structures
# Follow Cycle's autosuggestions for capitalization and step syntax
GIVEN I tYPE oPTIOn fOr "<MENU_NAME>" MEnu In tERminaL
# Separate things with a newline
# Capitalize keywords
# Capitalize "i"
Given i "do something"
giVEN i "do something"
gIvEN i "do something"
given			i 			"do something"

Feature: Group Test Feature File

@wip @sample
Scenario: Group Test Fun
If I verify number $number is greater than or equal to 100
	Then I echo "The Number " $number " is greater than 100!"
Elsif I verify number $number is greater than or equal to 0
	Then I echo "The Number " $number " is between 0 and 100"
Else I verify number $number is less than or equal to 0
	Then I echo "The Number " $number " is less than 0."
Endif

@wip
Scenario: i3PL Volume
Given I "login"
And I "access a report"
When I "make an update"
And I "close"

@wip
Scenario: HighJump Terminal Testing for Volume
Given I "open a terminal session"
While I "get data dynamically"
	And I "perform a process"
Endwhile

@wip
Scenario: Group Test Challenge 1
# Exercise: Make a Group Test of 5 'devices' in a While Loop that continues until a file does not exist
Given I verify file "<FILE_PATH>" exists

# 11:10am


Scenario: Check If File Exists

Given I wait $rfwait seconds
# Given I execute scenario "Stagger Management"

Given I "Get RF Device from CSV"
	When I verify variable "device" is assigned
	Then I echo "I am currently using RF Device: " $device

And I "Check if the File still exists"
	While I verify file "features\hello_world.feature" exists
		Then I echo "File still exists while using RF " $device 
        And I wait 10 seconds
	EndWhile

# poldat

Scenario: Challenges of Volume Testing
# 11:23AM
# Is there a usecase for my application?
# What challenges will we run into - and what are some solutions?

# Controller: Controls the flow of the volume test
# Hooks: A area where logic occurs in the controller
# Data Management
# Data Locks: write to a table to say I have this data
# Staggered Sessions: Don't Denial of Service attack unintentionally
# Start Test Check: Start all your tests at the same time
# End Test Check: Gracefully end your tests

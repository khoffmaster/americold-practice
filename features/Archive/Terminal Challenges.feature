Feature: Terminal Challenges

# Challenge 2
Scenario: Login, Navigate to Menu, Open Menu, Type Something In

# Use each of these steps at least once!

# 1. Screen Loaded
Given I verify screen is done loading in terminal within <NUMBER> seconds

# 2. Cursor Position
Given I see cursor at line <LINE_NUMBER> column <COLUMN_NUMBER> in terminal

# 3. Correct Screen
Given I see "" in terminal
Given I see "" on line <LINE_NUMBER> in terminal
Given I do not see "" in terminal

# 4. Always include a screen load check before copying text
Given I verify screen is done loading in terminal within <NUMBER> seconds
Given I copy terminal line 1 from column 1 through next 10 columns to variable "dog_name"

# Dog: Rufus
# Dg: Ruf...
# [select * from table where dog_name like 'ruf%'];

# Allocation (WMS) 
# Terminal screen will have a unique location

Then I echo $dog_name
Then I enter $dog_name in terminal

# 5. Move Cursor
Given I move cursor <LEFT|RIGHT|UP|DOWN> in terminal

# I Wait to troubleshoot
Given I wait 60 seconds


Scenario: Terminal Open
# Challenge: Find your connection information, use one of the below steps, and open your terminal and size it!
Given I open terminal connected to "serv-wm20191-rf.tryonsolutions.com:46063"
Then I wait 10 seconds
# Terminal ID: RDT001
# Username: jonathan
# Password: JONATHAN
Once I see cursor at line 2 column 1 in terminal
Then I type "RDT001" in terminal




# 1. Screen Loaded
Given I verify screen is done loading in terminal within <NUMBER> seconds

# 2. Cursor Position
Given I see cursor at line <LINE_NUMBER> column <COLUMN_NUMBER> in terminal

# 3. Correct Screen
Given I see "" in terminal


Given I close terminal

Given I reset terminal device "<DEVICE_CODE>" in warehouse "<WAREHOUSE_ID>"


Scenario: Traversal Best Practices
Once I verify screen is done loading in terminal within $max_response seconds
Once I see "<TEXT>" on line <LINE_NUMBER> in terminal
Once I see cursor at line <LINE_NUMBER> column <COLUMN_NUMBER> in terminal
Then I type option for "<MENU_NAME>" menu in terminal


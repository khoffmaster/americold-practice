Feature: Test Case

Background:
Given I import scenarios from "features/utilities/Utilities.feature"
And I import scenarios from "features/utilities/Number Utilities.feature"
And I execute scenario "Open Notepad"

After Scenario:
Then I execute scenario "Close Notepad"

@wip
Scenario: Type Giraffe in Notepad
And I assign "Giraffe" to variable "text"
When I execute scenario "Type in Notepad"

@wip
Scenario: Type Banana in Notepad
And I assign "Banana" to variable "text"
When I execute scenario "Type in Notepad"

@wip
Scenario: Type Orange in Notepad
And I assign "Orange" to variable "text"
When I execute scenario "Type in Notepad"

@wip
Scenario: Type Vanilla in Notepad
And I assign "Vanilla" to variable "text"
When I execute scenario "Type in Notepad"

@wip
Scenario Outline: Type in Notepad
Examples:
|text|a|
|"Giraffe"|1|
|"Banana"|0|
|"Orange"|1|
|"Vanilla"|5|

Given I assign <text> to variable "text"
When I execute scenario "Type in Notepad"
Then I echo <a>

@wip
Scenario: Type Modified Number in Notepad
Given I assign 5 to variable "original_number"
And I assign 10 to variable "modification"
And I execute scenario "Subtract Number"

When I assign $original_number to variable "text"
Then I execute scenario "Type in Notepad"

Scenario: Once Demonstration
Once I see image "Image:images/Notepad.png"
Feature: Web API Testing

# Application Programming Interface
# Endpoints designed by applications that you can send messages to and get messages back - for the purpose of using that information intentionally
# Google Maps API
# Waze API

@wip
Scenario: Web API Testing
# POST - sharing information
Given I http POST JSON to "<ENDPOINT>" "<JSON>" with headers "<HEADER_NAMES>"
Given I http POST JSON to "<ENDPOINT>" "<JSON>"

# GET - getting information
Given I http GET JSON from "<ENDPOINT>" with parameters "<PARAMETER_NAMES>"
Given I http GET JSON from "<ENDPOINT>" with headers "<HEADER_NAMES>"
Given I http GET JSON from "<ENDPOINT>"
Given I http GET JSON from "<ENDPOINT>" with parameters "<PARAMETER_NAMES>" and headers "<HEADER_NAMES>"

# PATCH - partial update to something
Given I http PATCH JSON to "<ENDPOINT>" "<JSON>" with headers "<HEADER_NAMES>"
Given I http PATCH JSON to "<ENDPOINT>" "<JSON>"

# DELETE - make a request to delete something
Given I http delete from "<ENDPOINT>"
Given I http DELETE from "<ENDPOINT>" with headers "<HEADER_NAMES>"

# PUT - update to something
Given I http PUT JSON to "<ENDPOINT>" "<JSON>" with headers "<HEADER_NAMES>"
Given I http PUT JSON to "<ENDPOINT>" "<JSON>"

# Response Management
Given I verify http response included header "<HEADER_NAME>"
Given I assign http response body to variable "<VARIABLE_NAME>"
Given I assign http response status code to variable "<VARIABLE_NAME>"
Given I assign http response headers to variables
Given I verify http response included header "<HEADER_NAME>" with a value of "<HEADER_VALUE>"
Given I verify http response had status code <STATUS_CODE>
Given I assign http response header "<HEADER_NAME>" to variable "<VARIABLE_NAME>"
Given I verify http response body was "<JSON>"

# Base URL
Given I set "<BASE_URL>" as my http base url

@wip
Scenario: Post Request Example
Given I http POST JSON to "<ENDPOINT>" "<JSON>"
# Given I verify http response had status code <STATUS_CODE>

@wip
Scenario: Exercise 1 - What are the three big HTTP Status codes?
# 9:26AM

# 400 - Bad Request/Syntax
# 500 - Internal Server Error
# 200 - OK - Successful - GET or POST request
# 201 - Successful POST or PUT request
# 401 - Unauthorized Request
# 403 - Forbidden
# 404 - Not Found
# 429 - Too many Requests

@wip
Scenario: Exercise 2 - What are Base URLs?
# 9:37AM
# Base URLs are the common portion of an API.

@wip
Scenario: Exercise 3 - Cat Facts!
# Set the Base URL for Cat Facts
# Get the JSON from Cat Facts
# Put the JSON into a variable
# Get a Cat Fact from that JSON
# 9:55AM
# Given I set "<BASE_URL>" as my http base url

Given I set "https://cat-fact.herokuapp.com" as my http base url

# GET - getting information
# Given I http GET JSON from "<ENDPOINT>" with parameters "<PARAMETER_NAMES>"
# Given I http GET JSON from "<ENDPOINT>" with headers "<HEADER_NAMES>"
# Given I http GET JSON from "<ENDPOINT>"
# Given I http GET JSON from "<ENDPOINT>" with parameters "<PARAMETER_NAMES>" and headers "<HEADER_NAMES>"

Given I http GET JSON from "/facts"

# Response Management
# Given I verify http response included header "<HEADER_NAME>"
# Given I assign http response body to variable "<VARIABLE_NAME>"
# Given I assign http response status code to variable "<VARIABLE_NAME>"
# Given I assign http response headers to variables
# Given I verify http response included header "<HEADER_NAME>" with a value of "<HEADER_VALUE>"
# Given I verify http response had status code <STATUS_CODE>
# Given I assign http response header "<HEADER_NAME>" to variable "<VARIABLE_NAME>"
# Given I verify http response body was "<JSON>"

Then I verify http response had status code 200
Given I assign http response body to variable "json"
And I echo $json

# JSON
# Given I assign value from JSON "<JSON>" with path "<LOCATOR_PATH>" to variable "<VARIABLE_NAME>"
# Given I verify count of elements from json "<JSON>" with path "<LOCATOR_PATH>" is equal to <NUMBER>
# Given I assign count of elements from json "<JSON>" with path "<LOCATOR_PATH>" to variable "<VARIABLE_NAME>"

Given I assign value from JSON $json with path "/all[7]/text" to variable "cat-fact-8"
And I echo $cat-fact-8

@wip
Scenario: Exercise 4 - What are Headers?
# 10:10am

# Additional meta-data passed in between messages that often includes authentication information

Scenario: Exercise 5 - How would one use Headers?
Given I http POST JSON to "/login" $authentication_json
And I verify http response had status code 200
Then I assign http response header "Set-Cookie" to variable "cookie"
And I assign "Example" to variable "anotherHeader"

Given I http GET JSON from "/topsecretstuff" with headers "cookie, anotherHeader"
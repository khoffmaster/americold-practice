Feature: Web Testing 101

@wip
Scenario: Open Web Browser
Given I open "Chrome" web browser

@wip
Scenario: Close Web Browser
Then I close web browser

Scenario: Process
Given I execute scenario "Open Web Browser"
When I navigate to "https://my.tryonsolutions.com/training/web/v1/" in web browser
# TODO: Validation

And I click element "className:webix_button" in web browser
Once I see "Add new order" in web browser

# $x("//input")[0]
And I type "Jonathan Yiv" in element "xPath:(//input)[1]" in web browser

And I type "1337" in element "xPath:(//input)[2]" in web browser

And I type "$100.92" in element "xPath:(//input)[4]" in web browser

# <span style="height:26px;padding-top:6px;" class="webix_input_icon wxi-calendar"></span>
# Pick May 6th for the Order Date
And I click element "className:wxi-calendar" in web browser
And I click element "text:Today" in web browser

# $x("//button[text()='Add']")
And I click element "xPath://button[text()='Add']" in web browser

# Challenge: Validate the order shows up in the table
# 11:26AM
And I see element "text:1337" in web browser

And I wait 10 seconds
Then I execute scenario "Close Web Browser"
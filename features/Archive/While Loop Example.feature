Feature: While Loop Example

@wip
Scenario: While Loop Example
Given I assign 0 to variable "count"
And I assign "TRUE" to variable "continue"

While I verify text $continue is equal to "TRUE"
	Then I echo "Hello!"
    And I increase variable "count" by 1
    If I verify number $count is equal to 7
    	Then I assign "FALSE" to variable "continue"
    Endif
EndWhile

Given I echo $count

Scenario: Nested While Loop Example
Given I assign 0 to variable "counter_one"
And I assign 0 to variable "total_counter"

While I verify number $counter_one is less than 10
    And I assign 0 to variable "counter_two"
	While I verify number $counter_two is less than 5
    	Then I increase variable "total_counter" by 1
        And I increase variable "counter_two" by 1
    Endwhile
    And I increase variable "counter_one" by 1
Endwhile

Given I echo $total_counter 
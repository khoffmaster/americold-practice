Feature: xPath Exercises

Scenario: xPath
# // - Relative xPath
# / - Absolute xPath

# //element
## a
## div
## ul
## li
## p
## header
## body
## footer


# $x("//input[@id='loginPassword']")
# ("//input[@id='loginPassword']")
# "//input[@id='loginPassword']"
# //input[@id='loginPassword']
Given I click element "xPath://input[@id='loginPassword']" in web browser

# $x("//input[contains(@id, 'jdaSearchField')]")
# //input[contains(@id, 'jdaSearchField')]
Feature: Git Bash

Scenario: Exercise 1 - Navigate to your Cycle Directory
Given I "have a directory at C:\Users\jyiv\Documents\Cycle Foundations\CycleFoundations"
And I "can use 'cd' to change directories/folders"
And I "can use 'ls' to list the directories/files in my current directory/folder"
And I "can use 'cd ..' to move one folder/directory up!"

Then I "navigate to my folder"

Scenario: Exercise 2 - Create a Git Repository
Given I "run 'git init' in Git Bash"

Scenario: Exercise 3 - git status
Given I "run 'git status' in Git Bash"
And I "notice files that aren't added to the snapshot/commit are red"
And I "run 'git add .' in Git Bash"
And I "run 'git add filename.feature' in Git Bash"
And I "notice files that are added to the snapshot are green"
And I "run 'git status' again in Git Bash"

Scenario: Exercise 4 - Take a Snapshot - git commit
Given I "run 'git commit -m 'Hello World' in Git Bash"

Scenario: Exercise 5 - See History - git log
Given I "run 'git log' in Git Bash"

Scenario: Exercise 6 - Take another Snapshot/Commit
# 9:30 AM 
Given I "add a new File"
And I "put something in it"
And I "add the file using git add"
When I "take another snapshot/commit using 'git commit -m 'message''"
Then I "make sure another commit has been added using git log" 

Scenario: Exercise 7 - Complete many Commits
# 7 Commits in your Git Log
# 9:41am

Scenario: Exercise 8 - Create a BitBucket Account and create a Repository
# 9:50am
Given I "create a BitBucket Account at BitBucket.org"
Then I "create my own repository"

Scenario: Exercise 9 - Using the HTTPS address, push your local commits remote
# https://jyiv@bitbucket.org/jyiv/americold-practice.git
Given I "git remote add origin https://address"
And I "git push -u origin master"
# If your BitBucket is not showing Commits/Branches - click on Repository Settings and they might appear

Scenario: Exercise 10 - Get to 7 Commits, Make an 8th Commit, Push up your Changes 
# 10:42AM
